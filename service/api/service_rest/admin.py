from django.contrib import admin
from .models import ServiceAppointment, AppointmentStatus, Technician, AutomobileVO
# Register your models here.
@admin.register(ServiceAppointment)
class ServiceAppointmentAdmin(admin.ModelAdmin):
    list_display = ['vin', 'customer_name', 'appointment_time', 'technician', 'reason', 'status']

@admin.register(AppointmentStatus)
class AppointmentStatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = ['id', 'employee_name', 'employee_id']

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ['id', 'year', 'vin', 'color', 'import_href']