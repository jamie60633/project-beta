import React, {useState, useEffect} from 'react'

const AutomobileList = () => {
  const [automobiles, setAutomobiles] = useState([])
  const loadAutomobiles = async () => {
    const getAutomobilesUrl = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(getAutomobilesUrl)
    if (response.ok) {
      const data = await response.json()
      setAutomobiles(data.autos)
    }
  }
  useEffect(() => {
    loadAutomobiles()
  }, [])
  return (
    <>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((a) => (
            <tr key={a.id}>
              <td>{a.vin}</td>
              <td>{a.color.charAt(0).toUpperCase()}{a.color.slice(1)}</td>
              <td>{a.year}</td>
              <td>{a.model.name}</td>
              <td>{a.model.manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default AutomobileList