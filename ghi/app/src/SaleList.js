import React, { useState, useEffect } from 'react'

const SaleList = () => {
  const [fullSalesList, setFullSalesList] = useState([]);
  const [shownSalesList, setShownSalesList] = useState([]);
  // const [query, setQuery] = useState('');
  const loadSalesList = async () => {
    const getSalesListUrl = 'http://localhost:8090/api/sales/';
    const response = await fetch(getSalesListUrl);
    if (response.ok) {
      const data = await response.json();
      setFullSalesList(data.sales);
      setShownSalesList(data.sales);
    }
  };
  useEffect(() => {
    loadSalesList();
  }, []);
  const handleSearch = (e) => {
    // setQuery(e.target.value)
    setShownSalesList(fullSalesList.filter(sr => sr.sales_person.name.toLowerCase().includes(e.target.value.toLowerCase()) || sr.automobile.vin.toLowerCase().includes(e.target.value.toLowerCase()) || sr.customer.name.toLowerCase().includes(e.target.value.toLowerCase())));
  }


  return (
    <>
      <h1 className="mb-3">Sales History</h1>
      <div className="d-flex mb-5" >
        <input onChange={handleSearch} className="form-control mr-sm-2" type="search" placeholder="Search by Salesperson or VIN or Customer" aria-label="Search" />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Person</th>
            <th>Employee Number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {shownSalesList.map((s) => (
            <tr key={s.id}>
              <td>{s.sales_person.name}</td>
              <td>{s.sales_person.employee_number}</td>
              <td>{s.customer.name}</td>
              <td>{s.automobile.vin}</td>
              <td>${s.price}.00</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default SaleList