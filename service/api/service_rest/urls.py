from django.urls import path
from .views import api_service_appointments, api_service_appointment, api_technicians, api_technician, api_finish_service_appointment, api_cancel_service_appointment, api_book_service_appointment

urlpatterns = [
    path('service-appointments/', api_service_appointments,
         name='api_service_appointments'),
    path('vin/<str:vin>/service-appointments/', api_service_appointments,
         name='api_service_appointments_by_vin'),
    path('service-appointments/<int:pk>/',
         api_service_appointment, name='api_service_appointment'),
    path('service-appointments/<int:pk>/finish/',
         api_finish_service_appointment, name='api_finish_service_appointment'),
    path('service-appointments/<int:pk>/book/',
         api_book_service_appointment, name='api_book_service_appointment'),
    path('service-appointments/<int:pk>/cancel/',
         api_cancel_service_appointment, name='api_cancel_service_appointment'),
    path('technicians/', api_technicians, name='api_technicians'),
    path('technicians/<int:pk>/', api_technician, name='api_technician'),
]
