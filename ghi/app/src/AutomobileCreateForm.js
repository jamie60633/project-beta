import React, {useState, useEffect} from 'react'
import {useNavigate} from 'react-router-dom'

const AutomobileCreateForm = () => {
  const navigate = useNavigate()
  const [color, setColor] = useState('')
  const [year, setYear] = useState('')
  const [vin, setVin] = useState('')
  const [model, setModel] = useState('')
  const [models, setModels] = useState([])
  const [errorMessage, setErrorMessage] = useState('')

  const loadModels = async () => {
    const getModelsUrl = 'http://localhost:8100/api/models/'
    const response = await fetch(getModelsUrl)
    if (response.ok) {
      const data = await response.json()
      setModels(data.models)
    }
  }

  useEffect(() => {loadModels()}, [])

  const handleSubmit = async (e) => {
    e.preventDefault()
    const postAutomobileUrl = 'http://localhost:8100/api/automobiles/'
    const data = {
      color,
      year,
      vin,
      model_id: model
    }
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    }
    const response = await fetch(postAutomobileUrl, fetchConfig)
    if (response.ok) {
      // const newAutomobile = await response.json()
      // console.log(newAutomobile)
      navigate('/inventories/automobiles')
    }
    else {
      const error = await response.json()
      setErrorMessage(error.detail)
    }
    setColor('')
    setYear('')
    setVin('')
    setModel('')
    
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a automobile</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setColor(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setYear(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={year}
                placeholder="Year"
                required
                type="number"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setVin(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={vin}
                maxLength="17"
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="year">VIN</label>
            </div>
            <div className="mb-3">
              <select
                onChange={(e) => setModel(e.target.value)}
                value={model}
                required
                name="model"
                id="model"
                className="form-select"
              >
                <option value="">Choose a model</option>
                {models.map((m) => (
                  <option key={m.id} value={m.id}>
                    {m.name}
                  </option>
                ))}
              </select>
            </div>
            <div
              className={`alert alert-danger mb-4 ms-2 ${
                errorMessage ? '' : 'd-none'
              }`}
              id="error-message"
            >
              {errorMessage}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AutomobileCreateForm