import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import ManufacturerList from './ManufacturerList';
import ManufacturerCreateForm from './ManufacturerCreateForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelCreateForm from './VehicleModelCreateForm';
import AutomobileList from './AutomobileList';
import AutomobileCreateForm from './AutomobileCreateForm';

import SaleList from './SaleList';
import SaleCreateForm from './SaleCreateForm';
import SalespersonCreateForm from './SalespersonCreateForm';
import CustomerCreateForm from './CustomerCreateForm';
import ServiceAppointmentList from './ServiceAppointmentList';
import ServiceAppointmentCreateForm from './ServiceAppointmentCreateForm';
import TechnicianCreateForm from './TechnicianCreateForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/inventories"> 
            <Route path="manufacturers"> 
              <Route path="" element={<ManufacturerList />} />
              <Route path="new" element={<ManufacturerCreateForm />} />
            </Route>
            <Route path="vehicle-models"> 
              <Route path="" element={<VehicleModelList />} />
              <Route path="new" element={<VehicleModelCreateForm />} />
            </Route>
            <Route path="automobiles"> 
              <Route path="" element={<AutomobileList />} />
              <Route path="new" element={<AutomobileCreateForm />} />
            </Route>
          </Route>
          <Route path="/sales">
            <Route path="" element={<SaleList />} />
            <Route path="new" element={<SaleCreateForm />} />
            <Route path="salespersons">
              <Route path="new" element={<SalespersonCreateForm />} />
            </Route>
            <Route path="customers">
              <Route path="new" element={<CustomerCreateForm />} />
            </Route>
          </Route>
          <Route path="/services">
            <Route path="" element={<ServiceAppointmentList />} />
            <Route path="new" element={<ServiceAppointmentCreateForm />} />
            <Route path="technicians">
              <Route path="new" element={<TechnicianCreateForm />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
