import React, { useEffect, useState } from 'react';

const ManufacturerList = () => {
  const [manufacturers, setManufacturers] = useState([]);
  const loadManufacturers = async () => {
    const getManufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(getManufacturerUrl);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    loadManufacturers();
  }, []);
  return (
    <>
      <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((m) => (
            <tr key={m.id}>
              <td>{m.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default ManufacturerList;
