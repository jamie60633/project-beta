import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const TechnicianCreateForm = () => {
  const navigate = useNavigate();
  const [employeeId, setEmployeeId] = useState('');
  const [employeeName, setEmployeeName] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const postTechnicianUrl = 'http://localhost:8080/api/technicians/';
    const data = {
      employee_id: employeeId,
      employee_name: employeeName,
    }
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    }
    const response = await fetch(postTechnicianUrl, fetchConfig);
    if (response.ok) {
      // const newTechnician = await response.json();
      // console.log(newTechnician)
      navigate(`/services`);
    }
    else {
      const error = await response.json();
      // console.log(error)
      setErrorMessage(error.detail);
    }
    setEmployeeId('');
    setEmployeeName('');
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setEmployeeId(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={employeeId}
                placeholder="Employee ID"
                required
                type="text"
                employee_id="employee_id"
                id="employee_id"
                className="form-control"
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setEmployeeName(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={employeeName}
                placeholder="Employee Name"
                required
                type="text"
                employee_id="employee_name"
                id="employee_name"
                className="form-control"
              />
              <label htmlFor="employee_name">Employee Name</label>
            </div>
            <div
              className={`alert alert-danger mb-4 ms-2 ${
                errorMessage ? '' : 'd-none'
              }`}
              id="error-message"
            >
              {errorMessage}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default TechnicianCreateForm