import React, { useState, useEffect } from 'react';

const VehicleModelList = () => {
  const [vehicleModels, setVehicleModels] = useState([]);

  const loadVehicleModels = async () => {
    const getVehicleModelsUrl = 'http://localhost:8100/api/models/';
    const response = await fetch(getVehicleModelsUrl);
    if (response.ok) {
      const data = await response.json();
      setVehicleModels(data.models);
    }
  };
  useEffect(() => {
    loadVehicleModels();
  }, []);

  return (
    <>
      <h1>Vehicle Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map((v) => (
            <tr key={v.id}>
              <td>{v.name}</td>
              <td>{v.manufacturer.name}</td>
              <td>
                <img
                  className="img-fluid"
                  style={{ height: '90px', width: '180px'}}
                  src={v.picture_url}
                  alt="model"
                ></img>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default VehicleModelList;
