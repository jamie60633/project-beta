import { NavLink } from 'react-router-dom';
function MainPage() {
  return (
    <div
      className="px-4 py-5 my-5 text-center"
      style={{
        backgroundImage:
          "url('https://images.pexels.com/photos/7918316/pexels-photo-7918316.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2')",
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        height: '90vh',
        borderRadius: '10px',
      }}
    >
      <h1 className="display-5 fw-bold text-white mt-5">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4 mt-5 text-white">
          The premiere solution for automobile dealership management!
        </p>
      </div>
      <div
        style={{
          marginTop: 100,
          width: '100%',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
        }}
      >
        <NavLink to="/sales" className="btn btn-success btn-lg me-5">
          SALES
        </NavLink>
        <NavLink to="/services" className="btn btn-success btn-lg ms-5">
          SERVICES
        </NavLink>
      </div>
    </div>
  );
}

export default MainPage;
